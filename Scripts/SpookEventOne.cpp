#include "SpookEventOne.hpp"
#include "LampController.hpp"
#include "Player.hpp"
#include "../LevelManager.hpp"

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Audio/Sound.h>
#include <Urho3D/Audio/SoundSource.h>
#include <Urho3D/Audio/SoundSource3D.h>
#include <Urho3D/Resource/ResourceCache.h>



namespace Game {
void SpookEventOne::Start() {
    GetNode()->GetParent()->GetComponents<LampController>(lamps, true);
}

void SpookEventOne::FixedUpdate(float) {
    // Make sure stage is still in range
    if (stage > end) {
        Remove();
        return;
    }
    // Manage durations
    if (stageState == running && (stage >= durations.size() || timer.GetMSec(false) > durations[stage])) {
        stageState = deinitialization;
    }
    // Stage-dependent code
    switch (stage) {
    case lightShapeChange: {
        switch (stageState) {
        case initialization: {
            // Set "die" light shape
            auto texture = GetSubsystem<ResourceCache>()->GetResource<Texture2D>("Textures/die.png");
            for (auto& lamp : lamps) {
                for (auto& light : lamp->getLights()) {
                    light.source->SetShapeTexture(texture);
                }
            }
            // Play sudden sound
            auto soundSource = GetNode()->CreateComponent<SoundSource>();
            soundSource->SetAutoRemoveMode(AutoRemoveMode::REMOVE_COMPONENT);
            soundSource->Play(GetSubsystem<ResourceCache>()->GetResource<Sound>("SFX/Sudden1.ogg"));
        } break;
        case deinitialization: {
            // Revert light shape
            for (auto& lamp : lamps) {
                for (auto& light : lamp->getLights()) {
                    light.source->SetShapeTexture(nullptr);
                }
            }
        } break;
        default: {}
        }
    } break;
    case lightFlickerLoudSteps: {
        switch (stageState) {
        case initialization: {
            // Start light flicker
            for (auto& lamp : lamps) {
                lamp->GetNode()->AddTag("HeavyFlicker");
                lamp->GetNode()->CreateComponent("LampFlicker");
            }
            // Find node to play loud steps from
            eastl::vector<Node *> res;
            GetScene()->GetNodesWithTag(res, "LoudStepsPlayer");
            // Play stair sound
            auto soundSource = res[0]->CreateComponent<SoundSource3D>();
            soundSource->SetAutoRemoveMode(AutoRemoveMode::REMOVE_COMPONENT);
            soundSource->SetGain(0.5f);
            soundSource->SetFarDistance(40.0f);
            soundSource->Play(GetSubsystem<ResourceCache>()->GetResource<Sound>("SFX/LoudSteps.ogg"));
        } break;
        case deinitialization: {
            // Stop light flicker
            for (auto& lamp : lamps) {
                lamp->GetNode()->RemoveComponent("LampFlicker");
            }
        } break;
        default: {}
        }
    } break;
    case lightsOffBoom: {
        switch (stageState) {
        case initialization: {
            // Turn off all lamps
            eastl::vector<LampController *> controllers;
            GetScene()->GetComponents<LampController>(controllers, true);
            for (auto lamp : controllers) {
                lamp->turnOff();
            }
            // Play power drain sound effect
            auto soundSource = GetNode()->CreateComponent<SoundSource>();
            soundSource->SetAutoRemoveMode(AutoRemoveMode::REMOVE_COMPONENT);
            soundSource->Play(GetSubsystem<ResourceCache>()->GetResource<Sound>("SFX/PowerDrain.ogg"));
        } break;
        default: {}
        }
    } break;
    case flashlightOn: {
        // Turn on all players flashlights
        for (auto player : GetGlobalVar("LevelManager").GetCustom<LevelManager*>()->players) {
            player->getFlashlight()->SetEnabled(true);
        }
        // Start horror ambience
        auto soundSource = GetNode()->CreateComponent<SoundSource>();
        soundSource->SetAutoRemoveMode(AutoRemoveMode::REMOVE_COMPONENT);
        soundSource->Play(GetSubsystem<ResourceCache>()->GetResource<Sound>("SFX/HorrorAmbience.ogg"));
    }
    }
    // Final stuff
    if (stageState == deinitialization) {
        stage++;
        stageState = initialization;
        timer.Reset();
    } else if (stageState == initialization) {
        stageState = running;
    }
}
}
