#pragma once

namespace Game {
class Trigger;
}
#ifndef TRIGGER_HPP
#define TRIGGER_HPP
#include "../easyscript/Namespace.hpp"

#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Core/Timer.h>



namespace Game {
class Player;

class Trigger final : public LogicComponent {
    URHO3D_OBJECT(Trigger, LogicComponent);

    Timer timer;
    const eastl::vector<Player *> *players;

public:
    using LogicComponent::LogicComponent;

    void Start() override;
    void FixedUpdate(float) override;
};
}
#endif // TRIGGER_HPP
