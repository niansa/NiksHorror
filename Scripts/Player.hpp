#pragma once

namespace Game {
class Player;
}
#ifndef PLAYER_HPP
#define PLAYER_HPP
#include "../easyscript/Namespace.hpp"

#include <Urho3D/Input/Input.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Physics/KinematicCharacterController.h>
#include <Urho3D/Physics/PhysicsEvents.h>
#include <Urho3D/Physics/PhysicsWorld.h>



namespace Game {
class LevelManager;

class Player final : public LogicComponent {
    URHO3D_OBJECT(Player, LogicComponent);

    static constexpr float walkSpeed = 1.6f,
                           sprintSpeed = 3.0f;

    LevelManager *levelManager;
    Node *head;
    Node *handP;
    Node *hand = nullptr;
    float headHeight;
    float headBob = 0.0f;

    CollisionShape* collisionShape;
    KinematicCharacterController* kinematicController;

public:
    using LogicComponent::LogicComponent;

    void Start() override;
    void FixedUpdate(float) override;
    void Update(float) override;

    Node *getHand() const {
        return hand;
    }
    Node *getHead() const {
        return head;
    }

    void teleport(const Vector3& offset);
    Light *getFlashlight() const;
};
}
#endif // PLAYER_HPP
