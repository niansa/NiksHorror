#include "Ghost.hpp"
#include "Player.hpp"
#include "../LevelManager.hpp"


#include <Urho3D/Graphics/AnimationController.h>



namespace Game {
void Ghost::Start() {
    // Get list of players
    players = &GetGlobalVar("LevelManager").GetCustom<LevelManager*>()->players;
    // Create and set up animation controller
    auto ani = GetNode()->CreateComponent<AnimationController>();
    ani->Play("Models/Monster/Animations/Monster_ArmatureArmatureAction.ani", 0, true);
}

void Ghost::FixedUpdate(float) {
    // Get first player
    auto player = (*players)[0];
    // Move towards it
    GetNode()->LookAt(player->GetNode()->GetWorldPosition());
    GetNode()->Translate(Vector3::FORWARD*0.01f);
    GetNode()->LookAt(player->head->GetWorldPosition());
}
}
