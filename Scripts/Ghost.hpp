#pragma once

namespace Game {
class Ghost;
}
#ifndef GHOST_HPP
#define GHOST_HPP
#include "../easyscript/Namespace.hpp"

#include <Urho3D/Scene/LogicComponent.h>



namespace Game {
class Player;

class Ghost final : public LogicComponent {
    URHO3D_OBJECT(Ghost, LogicComponent);

    eastl::vector<Player *> *players;

public:
    using LogicComponent::LogicComponent;

    void Start() override;
    void FixedUpdate(float) override;
};
}
#endif // GHOST_HPP
