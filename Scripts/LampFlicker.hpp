#pragma once

namespace Game {
class LampFlicker;
}
#ifndef LAMPFLICKER_HPP
#define LAMPFLICKER_HPP
#include "../easyscript/Namespace.hpp"

#include <Urho3D/Scene/Node.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Math/RandomEngine.h>



namespace Game {
class LampController;

class LampFlicker final : public LogicComponent {
    URHO3D_OBJECT(LampFlicker, LogicComponent);

    RandomEngine rng;
    bool heavyFlicker = false;
    LampController *controller;

public:
    using LogicComponent::LogicComponent;

    void Start() override;
    void Stop() override;
    void FixedUpdate(float) override;
};
}
#endif // LAMPFLICKER_HPP
