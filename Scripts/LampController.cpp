#include "LampController.hpp"
#include "../LevelManager.hpp"
#include "Player.hpp"

#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Audio/Sound.h>
#include <Urho3D/Resource/ResourceCache.h>



namespace Game {
void LampController::Start() {
    // Find lights
    eastl::vector<Light *> lightSources;
    GetNode()->GetComponents<Light>(lightSources, true);
    lights.reserve(lightSources.size());
    for (auto lightSource : lightSources) {
        lights.push_back({lightSource, lightSource->GetBrightness()});
    }
    // Find lit material
    if (GetNode()->HasComponent<StaticModel>()) {
        auto model = GetNode()->GetComponent<StaticModel>();
        for (litMatIdx = 0; litMatIdx != model->GetNumGeometries(); litMatIdx++) {
            auto mat = model->GetMaterial(litMatIdx);
            if (mat->GetShaderParameter("MatEmissiveColor").GetColor() == Color(1.0f, 1.0f, 1.0f)) {
                litMat = mat->Clone();
                break;
            }
        }
    }
    // Create sound source
    soundSource = GetNode()->CreateComponent<SoundSource3D>();
    soundSource->SetNearDistance(0.2f);
    soundSource->SetFarDistance(6.0f);
    soundSource->Play(GetSubsystem<ResourceCache>()->GetResource<Sound>("SFX/Lamp.ogg"));
    // Turn on by default if not specified otherwise
    if (!GetNode()->HasTag("DefaultOff")) {
        turnOn();
    } else {
        turnOff();
    }
}

void LampController::turnOn() {
    // Enable light sources
    for (auto light : lights) {
        light.source->SetEnabled(true);
    }
    // Enable lit material
    if (litMat) {
       GetNode()->GetComponent<StaticModel>()->SetMaterial(litMatIdx, litMat);
    }
    // Disable shadpw casting
    if (GetNode()->HasComponent<StaticModel>()) {
        GetNode()->GetComponent<StaticModel>()->SetCastShadows(false);
    }
    // Enable sound source
    soundSource->SetEnabled(true);
}

void LampController::turnOff() {
    // Disable light sources
    for (auto light : lights) {
        light.source->SetEnabled(false);
    }
    // Disable lit material
    if (litMat) {
        GetNode()->GetComponent<StaticModel>()->SetMaterial(litMatIdx, nullptr);
    }
    // Enable shadpw casting
    if (GetNode()->HasComponent<StaticModel>()) {
        GetNode()->GetComponent<StaticModel>()->SetCastShadows(true);
    }
    // Disable sound source
    soundSource->SetEnabled(false);
}

void LampController::setBrightness(float off) {
    // Adjust light brightness
    for (auto light : lights) {
        light.source->SetBrightness(Abs(light.brightness+off));
    }
    // Adjust lit material
    if (litMat) {
        litMat->SetShaderParameter("MatEmissiveColor", Color(1.0f+off, 1.0f+off, 1.0f+off));
    }
    // Adjust sound gain
    soundSource->SetGain(1.0f+off*1.5f);
}
}
