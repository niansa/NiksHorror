#pragma once

namespace Game {
class LampController;
}
#ifndef LAMPCONTROLLER_HPP
#define LAMPCONTROLLER_HPP
#include "../easyscript/Namespace.hpp"

#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Audio/SoundSource3D.h>



namespace Game {
struct OriginalLight {
    Light *source;
    float brightness;
};

class LampController final : public LogicComponent {
    URHO3D_OBJECT(LampController, LogicComponent);

    eastl::vector<OriginalLight> lights;
    unsigned litMatIdx;
    SharedPtr<Material> litMat = nullptr;
    SoundSource3D *soundSource;

public:
    using LogicComponent::LogicComponent;

    void Start() override;

    const eastl::vector<OriginalLight>& getLights() const {
        return lights;
    }
    bool isTurnedOn() const {
        return lights[0].source->IsEnabled();
    }

    void turnOn();
    void turnOff();
    void setBrightness(float off);
};
}
#endif // LAMPCONTROLLER_HPP
