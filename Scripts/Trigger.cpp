#include "Trigger.hpp"
#include "../LevelManager.hpp"
#include "Player.hpp"

#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/StaticModel.h>



namespace Game {
void Trigger::Start() {
    // Get players vector
    players = &GetGlobalVar("LevelManager").GetCustom<LevelManager*>()->players;
}

void Trigger::FixedUpdate(float) {
    if (timer.GetMSec(false) > 1000) {
        auto bbox = GetNode()->GetComponent<StaticModel>()->GetWorldBoundingBox();
        for (auto player : *players) {
            auto wpos = player->GetNode()->GetWorldPosition();
            if (bbox.IsInside(player->GetNode()->GetWorldPosition()) == Intersection::INSIDE) {
                GetNode()->CreateComponent(GetNode()->GetVar("Creates").GetString());
                Remove();
                return;
            }
        }
    }
}
}
