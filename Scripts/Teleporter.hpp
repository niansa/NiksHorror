#pragma once

namespace Game {
class Teleporter;
}
#ifndef TELEPORTER_HPP
#define TELEPORTER_HPP
#include "../easyscript/Namespace.hpp"

#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Core/Timer.h>



namespace Game {
class Player;

class Teleporter final : public LogicComponent {
    URHO3D_OBJECT(Teleporter, LogicComponent);

    Timer timer;
    const eastl::vector<Player *> *players;

public:
    using LogicComponent::LogicComponent;

    void Start() override;
    void FixedUpdate(float) override;
};
}
#endif // TELEPORTER_HPP
