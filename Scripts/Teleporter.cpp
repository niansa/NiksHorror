#include "Teleporter.hpp"
#include "../LevelManager.hpp"
#include "Player.hpp"

#include <Urho3D/Scene/Node.h>



namespace Game {
void Teleporter::Start() {
    // Get players vector
    players = &GetGlobalVar("LevelManager").GetCustom<LevelManager*>()->players;
}

void Teleporter::FixedUpdate(float) {
    if (timer.GetMSec(false) > 1000) {
        auto destination = GetNode()->GetVar("Destination").GetVector3();
        for (auto player : *players) {
            if ((GetNode()->GetWorldPosition() - player->GetNode()->GetWorldPosition()).Length() < 2.5f) {
                player->teleport(destination);
                if (GetNode()->HasTag("Once")) {
                    Remove();
                    return;
                }
            }
        }
        timer.Reset();
    }
}
}
