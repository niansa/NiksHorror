#pragma once

namespace Game {
class SpookEventOne;
}
#ifndef SPOOKEVENTONE_HPP
#define SPOOKEVENTONE_HPP
#include "../easyscript/Namespace.hpp"

#include <Urho3D/Scene/Node.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Core/Timer.h>



namespace Game {
class LampController;

class SpookEventOne final : public LogicComponent {
    URHO3D_OBJECT(SpookEventOne, LogicComponent);

    enum : uint8_t {
        lightShapeChange,
        lightFlickerLoudSteps,
        lightsOffBoom,
        flashlightOn,
        start = lightShapeChange,
        end = flashlightOn
    };
    uint8_t stage = start;
    static constexpr eastl::array<unsigned, end> durations = {
        2500,
        5000,
        1500
    };
    enum {
        initialization,
        running,
        deinitialization
    } stageState = initialization;

    eastl::vector<LampController *> lamps;
    Timer timer;

public:
    using LogicComponent::LogicComponent;

    void Start() override;
    void FixedUpdate(float) override;
};
}
#endif // SPOOKEVENTONE_HPP
