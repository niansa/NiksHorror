#include "LampFlicker.hpp"
#include "LampController.hpp"



namespace Game {
void LampFlicker::Start() {
    // Get lamp controller
    controller = GetNode()->GetOrCreateComponent<LampController>();
    heavyFlicker = GetNode()->HasTag("HeavyFlicker");
}

void LampFlicker::Stop() {
    // Revert to full brightness
    controller->setBrightness(0.0f);
}

void LampFlicker::FixedUpdate(float) {
    if (rng.GetBool(0.25f)) {
        // Random flicker
        controller->setBrightness(rng.GetFloat(heavyFlicker ? -0.4f : -0.2f, 0.0f));
        // Make sure that lamp is still turned on
        if (!controller->isTurnedOn()) {
            // Remove the flicker
            Remove();
        }
    }
}
}
