#include "LevelManager.hpp"
#include "easyscript/Namespace.hpp"
#include "Scripts/Player.hpp"

#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/RenderPipeline/RenderPipeline.h>
#include <Urho3D/RenderPipeline/BloomPass.h>
#include <Urho3D/RenderPipeline/SceneProcessor.h>



namespace Game {
void LevelManager::setupLevel() {
    // Correct walls and floors
    for (auto& node : scene->GetChildren(true)) {
        if (node->HasComponent<StaticModel>()) {
            bool isFloor = node->HasTag("FloorSide");
            bool isWall = node->HasTag("WallSide");
            if (isWall || isFloor) {
                auto mesh = node->GetParent();
                auto model = node->GetComponent<StaticModel>();
                // Fix UVs
                auto originalMaterial = model->GetMaterial();
                if (originalMaterial) {
                    auto material = originalMaterial->Clone();
                    Vector4 voff(0.0f, 1.0f, 0.0f, 0.0f);
                    Vector4 uoff(1.0f, 0.0f, 0.0f, 0.0f);
                    auto voffo = material->GetShaderParameter("VOffset");
                    auto uoffo = material->GetShaderParameter("UOffset");
                    if (!voffo.IsEmpty() && !uoffo.IsEmpty()) {
                        voff = voffo.GetVector4();
                        uoff = uoffo.GetVector4();
                    } else {
                        voff = {0.0f, 1.0f, 0.0f, 0.0f};
                        uoff = {1.0f, 0.0f, 0.0f, 0.0f};
                    }
                    if (isFloor) {
                        voff.y_ *= mesh->GetScale().z_;
                        uoff.x_ *= mesh->GetScale().x_;
                    } else if (node->GetName() == "North" || node->GetName() == "South") {
                        node->SetRotation(Quaternion(node->GetRotation().EulerAngles()+Vector3(90.0f, 0.0f, 0.0f)));
                        voff.y_ *= mesh->GetScale().y_;
                        uoff.x_ *= mesh->GetScale().z_;
                    } else if (node->GetName() == "West" || node->GetName() == "East") {
                        voff.y_ *= mesh->GetScale().y_;
                        uoff.x_ *= mesh->GetScale().x_;
                    }
                    material->SetShaderParameter("VOffset", voff);
                    material->SetShaderParameter("UOffset", uoff);
                    model->SetMaterial(eastl::move(material));
                }
            }
        }
    }
    // Make the environment darker
    scene->FindChild("SceneMain/Sun")->Remove();
    auto env = scene->CreateChild("Environment");
    env->SetScale(Vector3(100.0f, 100.0f, 100.0f));
    auto zone = env->CreateComponent<Zone>();
    zone->SetAmbientBrightness(0.0f);
    // Apply graphics settings
    auto renderPipeline = scene->GetOrCreateComponent<RenderPipeline>();
    auto renderSettings = renderPipeline->GetSettings();
    renderSettings.bloom_.enabled_ = true;
    renderSettings.bloom_.intensity_ = 1.0f;
    renderSettings.sceneProcessor_.lightingMode_ = DirectLightingMode::Forward;
    renderSettings.sceneProcessor_.maxPixelLights_ = 20;
    renderSettings.renderBufferManager_.colorSpace_ = RenderPipelineColorSpace::LinearHDR;
    renderSettings.toneMapping_ = ToneMappingMode::Reinhard;
    renderSettings.shadowMapAllocator_.enableVarianceShadowMaps_ = true;
    renderSettings.shadowMapAllocator_.varianceShadowMapMultiSample_ = 5;
    renderPipeline->SetSettings(renderSettings);
    // Find players
    scene->GetComponents<Player>(players, true);
}
}
